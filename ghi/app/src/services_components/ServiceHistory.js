import { useEffect, useState } from 'react';

function ServiceHistoryList(){
    const [appointment, setAppointment] = useState([]);
    const [filterVin, setFilterVin] = useState("");

    const getData = async () => {
      const response = await fetch('http://localhost:8080/api/appointments/');

      if (response.ok) {
        const data = await response.json();
        setAppointment(data.appointment);
      };
    };

    useEffect(()=>{
        getData()
      }, []);

    const handleFilterVinChange = (e) => {
        const { value } = e.target;
        setFilterVin(value);
    };

    return (
        <>
        <h1 className="my-3">Service History</h1>
        <input onChange={handleFilterVinChange} value={filterVin} placeholder="Search by VIN" />
        <table className="table table-striped">
        <thead>
          <tr>
            <th>VIN</th>
            <th>Is VIP?</th>
            <th>Customer</th>
            <th>Date</th>
            <th>Technician</th>
            <th>Reason</th>
            <th>Status</th>
          </tr>
        </thead>
        <tbody>
          {appointment
          .filter((app) => app.vin.includes(filterVin.toUpperCase()))
          .map(app => {
            return (
              <tr key={app.id}>
                <td>{ app.vin }</td>
                <td> { (() => {
                    if (app.vip === true) {
                        return "Yes"
                    } else {
                        return "No"
                    }
                    }) () }
                </td>
                <td>{ app.customer }</td>
                <td>{ app.date_time }</td>
                <td>{ app.technician.first_name } { app.technician.last_name }</td>
                <td>{ app.reason }</td>
                <td>{ app.status }</td>
              </tr>
            );
          })}
        </tbody>
      </table>
      </>
    );
};

export default ServiceHistoryList;
