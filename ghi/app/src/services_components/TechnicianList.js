import { useEffect, useState } from 'react';

function TechnicianList(){
    const [technician, setTechnician] = useState([]);

    const getData = async () => {
      const response = await fetch('http://localhost:8080/api/technicians/');

      if (response.ok) {
        const data = await response.json();
        setTechnician(data.technician);
      };
    };

    useEffect(()=>{
        getData()
      }, []);

    return (
        <>
        <h1 className="my-3">Our Lovely Technicians</h1>
        <table className="table table-striped">
        <thead>
          <tr>
            <th>Technician No.</th>
            <th>First Name</th>
            <th>Last Name</th>
            <th>Employee ID</th>
          </tr>
        </thead>
        <tbody>
          {technician.sort((a,b) => a.id - b.id).map(tech => {
            return (
              <tr key={tech.id}>
                <td>{ tech.id }</td>
                <td>{ tech.first_name }</td>
                <td>{ tech.last_name }</td>
                <td>{ tech.employee_id }</td>
              </tr>
            );
          })}
        </tbody>
      </table>
      </>
    );
};

export default TechnicianList;
