import { useEffect, useState } from 'react';

function AppointmentList(){
    const [appointment, setAppointment] = useState([]);

    const getData = async () => {
      const response = await fetch('http://localhost:8080/api/appointments/');

      if (response.ok) {
        const data = await response.json();
        setAppointment(data.appointment);
      };
    };

    useEffect(()=>{
        getData();
      }, []);

    const handleCancelButton = async (e) => {
        const { id } = e.target;
        const response = await fetch(`http://localhost:8080/api/appointments/${id}/cancel/`, {
            method: "put"
        });

        if (response.ok) {
            const data = await response.json();
            getData();
        };
    };

    const handleFinishButton = async (e) => {
        const { id } = e.target;
        const response = await fetch(`http://localhost:8080/api/appointments/${id}/finish/`, {
            method: "put"
        });

        if (response.ok) {
            const data = await response.json();
            getData();
        };
    };

    return (
        <>
        <h1 className="my-3">Service Appointments</h1>
        <table className="table table-striped">
        <thead>
        <tr>
            <th>VIN</th>
            <th>Is VIP?</th>
            <th>Customer</th>
            <th>Date</th>
            <th>Technician</th>
            <th>Reason</th>
            <th>Status</th>
        </tr>
        </thead>
        <tbody>
        {appointment.filter(app => app.status == "created").map(app => {
            return (
            <tr key={app.id}>
                <td>{ app.vin }</td>
                <td> { (() => {
                    if (app.vip === true) {
                        return "Yes";
                    } else {
                        return "No";
                    }
                    }) () }
                </td>
                <td>{ app.customer }</td>
                <td>{ app.date_time }</td>
                <td>{ app.technician.first_name } { app.technician.last_name }</td>
                <td>{ app.reason }</td>
                <td>{ app.status }</td>
                <td>
                    <button type="button" className="btn btn-danger" id={app.id} onClick={handleCancelButton}>Cancel</button>
                    <button type="button" className="btn btn-success" id={app.id} onClick={handleFinishButton}>Finish</button>
                </td>

            </tr>
            );
        })}
        </tbody>
    </table>
    </>
    );
};

export default AppointmentList;
