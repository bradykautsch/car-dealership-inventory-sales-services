import { useEffect, useState } from 'react';

function CustomerList() {
    const [customers, setCustomers] = useState([]);

    const getData = async () => {
        const response = await fetch('http://localhost:8090/api/customers/');

        if (response.ok) {
            const data = await response.json();
            setCustomers(data.customers);
        };
    };

    useEffect(()=>{
        getData();
    }, []);

    return (
        <div className="list-container">
        <h2 className="list-title">Customers</h2>
        <table className="table table-striped">
          <thead>
            <tr>
              <th>First Name</th>
              <th>Last Name</th>
              <th>Phone Number</th>
              <th>Address</th>
            </tr>
          </thead>
          <tbody>
            {customers.sort((a,b) => a.id - b.id).map(customers => {
              return (
                <tr key={customers.id}>
                  <td> { customers.first_name }</td>
                  <td>{ customers.last_name }</td>
                  <td>{ customers.phone_number }</td>
                  <td>{ customers.address }</td>
                </tr>
              );
            })}
          </tbody>
        </table>
     </div>
    );
};

export default CustomerList;
