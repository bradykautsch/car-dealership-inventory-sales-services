import { useEffect, useState } from 'react';

function SalespersonHistory() {
    const [sales, setSales] = useState([]);
    const [salesperson, setSalesperson] = useState("");
    const [filterSalesperson, setFilterSalesperson] = useState([]);

    const getData = async () => {
        const response = await fetch('http://localhost:8090/api/sales/');

        if (response.ok) {
            const data = await response.json();
            setSales(data.sales);
        };
    };

    useEffect(()=>{
        getData();
    }, []);

    useEffect(()=>{
        if (salesperson) {
            const filterEntries = sales.filter(sale => sale.salesperson.employee_id === salesperson);
            setFilterSalesperson(filterEntries);
        } else {
            setFilterSalesperson(sales);
        };

    }, [salesperson, sales]);

    const handleSalespersonChange = (e) => {
        setSalesperson(e.target.value);
    };

    const uniqueNames = [];
    const filteredNames = [];

    for (let i =0; i < sales.length; i++) {
        const sale = sales[i];
        const fullName = `${sale.salesperson.first_name} ${sale.salesperson.last_name}`;

        if (!uniqueNames.includes(fullName)) {
            uniqueNames.push(fullName);
            filteredNames.push(sale);
        };
    };

    return (
        <div className="list-container">
        <h2 className="list-title">Salesperson History</h2>
        <select onChange={handleSalespersonChange} value={salesperson} name="salesperson" id="salesperson" required>
            <option value="">Select a salesperson</option>
            {filteredNames.map(sale => (
                <option key={sale.salesperson.employee_id} value={sale.salesperson.employee_id}>
                    {`${sale.salesperson.first_name} ${sale.salesperson.last_name}`}
                </option>
            ))};
        </select>
        <table className="table table-striped">
          <thead>
            <tr>
              <th>Salesperson Employee ID</th>
              <th>Salesperson Name</th>
              <th>Customer</th>
              <th>VIN</th>
              <th>Price</th>
            </tr>
          </thead>
          <tbody>
            {filterSalesperson.sort((a,b) => a.id - b.id).map(sale => {
              return (
                <tr key={sale.id}>
                  <td> { sale.salesperson.employee_id }</td>
                  <td>{ `${sale.salesperson.first_name} ${sale.salesperson.last_name}`}</td>
                  <td>{ `${sale.customer.first_name} ${sale.customer.last_name}`}</td>
                  <td>{ sale.automobile.vin }</td>
                  <td>{ sale.price }</td>
                </tr>
              );
            })}
          </tbody>
        </table>
    </div>
    );
};

export default SalespersonHistory;
