import React, {useState, useEffect } from 'react';

function AddVehicle() {
  const [manufacturers, setManufacturers] = useState([]);
  const [formData, setFormData] = useState({
    name: '',
    picture_url: '',
    manufacturer_id: '',
  });

  const getData = async () => {
    const url = 'http://localhost:8100/api/manufacturers/';
    const response = await fetch(url);

    if (response.ok) {
      const data = await response.json();
      setManufacturers(data.manufacturers);
    };
  };

  useEffect(() => {
    getData();
  }, []);

  const handleSubmit = async (event) => {
    event.preventDefault();

    const locationUrl = `http://localhost:8100/api/models/`;

    const fetchConfig = {
      method: "post",
      body: JSON.stringify(formData),
      headers: {
        'Content-Type': 'application/json',
      },
    };

    const response = await fetch(locationUrl, fetchConfig);

    const data = await response.json();

    if (response.ok) {
      setFormData({
        name: '',
        picture_url: '',
        manufacturer_id: '',
      });

    };
  };

  const handleChangeName = (e) => {
    const value = e.target.value;
    const inputName = e.target.name;
    setFormData({
      ...formData,
      [inputName]: value
    });
  };

  return (
    <>
    <h1 className='my-3'>Create a Model</h1>
    <div className="my-3">
      <div className="row">
        <div className="col">
          <div className="card shadow">
            <div className="card-body">

              <form onSubmit={handleSubmit} id="create-attendee-form">
                <div className="mb-3">
                  <select onChange={handleChangeName} value={formData.manufacturer_id} name="manufacturer_id" id="manufacturer_id" required>
                    <option value="manufacturer_id">Choose a manufacturer</option>
                    {
                      manufacturers.map(manufacturer => {
                        return (
                          <option key={manufacturer.href} value={manufacturer.id}>{manufacturer.name}</option>
                        )
                      })
                    };
                  </select>
                </div>
                <div className="col">
                  <div className="col">
                    <div className="form-floating mb-3">
                      <input onChange={handleChangeName} value={formData.name} required placeholder="Model name" type="text" id="name" name="name" className="form-control" />
                      <label htmlFor="name">Model name</label>
                    </div>
                  </div>
                  <div className="col">
                    <div className="form-floating mb-3">
                      <input onChange={handleChangeName} value={formData.picture_url} required placeholder="Picture URL" type="url" id="picture_url" name="picture_url" className="form-control" />
                      <label htmlFor="picture_url">Picture URL</label>
                    </div>
                  </div>
                </div>
                <button className="btn btn-lg btn-primary">Create!</button>
              </form>
            </div>
          </div>
        </div>
      </div>
    </div>
    </>
  );
};

export default AddVehicle;
