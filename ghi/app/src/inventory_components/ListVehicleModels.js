import { useEffect, useState } from 'react';

function VehicleModelsList() {
    const [models, setModels] = useState([]);

    const getData = async () => {
        const response = await fetch('http://localhost:8100/api/models/');

        if (response.ok) {
            const data = await response.json();
            setModels(data.models);
        };
    };

    useEffect(()=>{
        getData();
    }, []);

    return (
        <div className="list-container">
        <h2 className="list-title">Models</h2>
        <table className="table table-striped">
          <thead>
            <tr>
              <th>Name</th>
              <th>Manufacturer</th>
              <th>Picture</th>
            </tr>
          </thead>
          <tbody>
            {models.sort((a,b) => a.name.localeCompare(b.name)).map(models => {
              return (
                <tr key={models.id}>
                  <td> { models.name }</td>
                  <td> { models.manufacturer.name }</td>
                  <td><img src={ models.picture_url } style={{ width: '200px', height:'auto' }}/></td>
                </tr>
              );
            })}
          </tbody>
        </table>
     </div>
    );
};

export default VehicleModelsList;
