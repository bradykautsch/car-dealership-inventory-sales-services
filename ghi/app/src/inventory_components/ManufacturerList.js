import { useEffect, useState } from 'react';

function ManufacturerList() {
    const [manufacturers, setManufacturers] = useState([]);

    const getData = async () => {
        const response = await fetch('http://localhost:8100/api/manufacturers/');

        if (response.ok) {
            const data = await response.json();
            setManufacturers(data.manufacturers);
        };
    };

    useEffect(()=>{
        getData()
    }, []);

    return (
        <div className="list-container">
        <h2 className="list-title">Manufacturers</h2>
        <table className="table table-striped">
          <thead>
            <tr>
              <th>Name</th>
            </tr>
          </thead>
          <tbody>
            {manufacturers.sort((a,b) => a.name.localeCompare(b.name)).map(manufacturers => {
              return (
                <tr key={manufacturers.id}>
                  <td> { manufacturers.name }</td>
                </tr>
              );
            })}
          </tbody>
        </table>
     </div>
    );
};

export default ManufacturerList;
