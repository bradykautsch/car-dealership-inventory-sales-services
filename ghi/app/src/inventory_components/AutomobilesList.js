import { useEffect, useState } from 'react';


function AutomobilesList() {
    const [autos, setAutos] = useState([]);
    const [sales, setSales] = useState([]);

    const getData = async () => {
        const autosResponse = await fetch('http://localhost:8100/api/automobiles/');
        const salesResponse = await fetch('http://localhost:8090/api/sales/');

        if (autosResponse.ok && salesResponse.ok) {
            const autosData = await autosResponse.json();
            const salesData = await salesResponse.json();

            setAutos(autosData.autos);
            setSales(salesData.sales);
        };
    };

    useEffect(()=>{
        getData();
    }, []);

    const saleVins = []
    for(let sale of sales){
      saleVins.push(sale.automobile.vin)
    }

    return (
        <div className="list-container">
        <h2 className="list-title">Automobiles</h2>
        <table className="table table-striped">
          <thead>
            <tr>
              <th>VIN</th>
              <th>Color</th>
              <th>Year</th>
              <th>Model</th>
              <th>Manufacturer</th>
              <th>Sold</th>
            </tr>
          </thead>
          <tbody>
            {autos.map(auto => {
              return (
                <tr key={auto.id}>
                  <td> { auto.vin }</td>
                  <td> { auto.color }</td>
                  <td> { auto.year }</td>
                  <td> { auto.model.name }</td>
                  <td> { auto.model.manufacturer.name }</td>            
                  <td> { (() => {
                    if (saleVins.includes(auto.vin)) {
                        return "Yes";
                    } else {
                        return "No";
                    }
                    }) () }
                  </td>
                </tr>
              );
            })}
          </tbody>
        </table>
     </div>
    );
};

export default AutomobilesList
