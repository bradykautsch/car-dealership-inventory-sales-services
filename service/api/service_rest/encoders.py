from common.json import ModelEncoder
from .models import Technician, AutomobileVO, Appointment

class TechnicianListEncoder(ModelEncoder):
    model = Technician
    properties = ["id",
                  "first_name",
                  "last_name",
                  "employee_id"]

class AutomobileVOEncoder(ModelEncoder):
    model = AutomobileVO
    properties = ["vin",
                  "import_href",
                  "sold"]

class AppointmentListEncoder(ModelEncoder):
    model = Appointment
    properties = ["id",
                  "date_time",
                  "reason",
                  "status",
                  "vin",
                  "customer",
                  "vip",
                  "technician"]
    encoders = {
        "technician": TechnicianListEncoder(),
    }
