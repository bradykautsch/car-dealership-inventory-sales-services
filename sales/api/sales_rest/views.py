from django.shortcuts import render
from .encoders import SalespersonEncoder, CustomerEncoder, SaleEncoder
from .models import AutomobileVO, Salesperson, Customer, Sale
from django.views.decorators.http import require_http_methods
from django.http import JsonResponse
import json


####################### GET LIST/CREATE SALESPEOPLE ###############################

@require_http_methods(["GET", "POST"])
def api_list_salespeople(request):
    if request.method == "GET":
        try:
            salespeople = Salesperson.objects.all()
            return JsonResponse(
                {"salespeople": salespeople},
                encoder=SalespersonEncoder
            )
        except Salesperson.DoesNotExist:
            return JsonResponse(
                {"message": "Salesperson(s) Not Found"}, status=404
            )
    else:
        content = json.loads(request.body)
        new_salesperson = Salesperson.objects.create(**content)
        return JsonResponse(
            new_salesperson,
            encoder=SalespersonEncoder,
            safe=False,
        )

####################### SHOW DETAILS/DELETE/UPDATE SALESPERSON ###############################

@require_http_methods(["DELETE", "GET", "PUT"])
def api_show_salesperson(request, pk):
    if request.method == "GET":
        try:
            salesperson = Salesperson.objects.get(id=pk)
            return JsonResponse(
                salesperson,
                encoder=SalespersonEncoder,
                safe=False
            )
        except Salesperson.DoesNotExist:
            return JsonResponse(
                {"message": "Salesperson Not Found"}, status=404
            )
    elif request.method == "DELETE":
        try:
            salesperson=Salesperson.objects.get(id=pk)
            salesperson.delete()
            return JsonResponse(
                salesperson,
                encoder=SalespersonEncoder,
                safe=False,
            )
        except Salesperson.DoesNotExist:
            return JsonResponse(
                {"message": "Salesperson Not Found; Could not DELETE"}, status=404
            )
    else:
        try:
            content = json.loads(request.body)
            salesperson = Salesperson.objects.get(id=pk)
            props = ["first_name", "last_name", "employee_id"]

            for prop in props:
                if prop in content:
                    setattr(salesperson, prop, content[prop])
            salesperson.save()
            return JsonResponse(
                salesperson,
                encoder=SalespersonEncoder,
                safe=False,
            )
        except Salesperson.DoesNotExist:
            response = JsonResponse({"message": "Salesperson does not exist; Could not UPDATE"})
            response.status_code = 404
            return response

####################### GET LIST/CREATE CUSTOMERS ###############################

@require_http_methods(["GET", "POST"])
def api_list_customers(request):
    if request.method == "GET":
        try:
            customers = Customer.objects.all()
            return JsonResponse(
                {"customers": customers},
                encoder=CustomerEncoder
            )
        except Customer.DoesNotExist:
            return JsonResponse(
                {"message": "Customers Not Found"}, status=404
            )
    else:
        content = json.loads(request.body)
        new_customer = Customer.objects.create(**content)
        return JsonResponse(
            new_customer,
            encoder=CustomerEncoder,
            safe=False,
        )

####################### SHOW DETAILS/DELETE/UPDATE CUSTOMER ###############################

@require_http_methods(["DELETE", "GET", "PUT"])
def api_show_customers(request, pk):
    if request.method == "GET":
        try:
            customer = Customer.objects.get(id=pk)
            return JsonResponse(
                customer,
                encoder=CustomerEncoder,
                safe=False
            )
        except Customer.DoesNotExist:
            return JsonResponse(
                {"message": "Customer Not Found"}, status=404
            )
    elif request.method == "DELETE":
        try:
            customer=Customer.objects.get(id=pk)
            customer.delete()
            return JsonResponse(
                customer,
                encoder=CustomerEncoder,
                safe=False,
            )
        except Customer.DoesNotExist:
            return JsonResponse(
                {"message": "Customer Not Found; Could not DELETE"}, status=404
            )
    else:
        try:
            content = json.loads(request.body)
            customer = Customer.objects.get(id=pk)
            props = ["first_name", "last_name", "address", "phone_number"]

            for prop in props:
                if prop in content:
                    setattr(customer, prop, content[prop])
            customer.save()
            return JsonResponse(
                customer,
                encoder=CustomerEncoder,
                safe=False,
            )
        except Customer.DoesNotExist:
            return JsonResponse(
                {"message": "Customer Not Found; Could not UPDATE"}, status=404
                )

####################### GET LIST/CREATE SALES ###############################

@require_http_methods(["GET", "POST"])
def api_list_sales(request):
    if request.method == "GET":
        try:
            sales = Sale.objects.all()
            return JsonResponse(
                {"sales": sales},
                encoder=SaleEncoder
            )
        except Sale.DoesNotExist:
            return JsonResponse(
                {"message": "Sale Object Not Found"}, status=404
            )
    else:
        try:
            content = json.loads(request.body)
            automobile = AutomobileVO.objects.get(vin=content["automobile"])
            salesperson = Salesperson.objects.get(id=content["salesperson"])
            customer = Customer.objects.get(id=content["customer"])
            price = content["price"]
            sold = content["sold"]
            new_sale = Sale(
                automobile=automobile,
                salesperson=salesperson,
                customer=customer,
                price=price,
                sold=sold
            )
            vin = content["automobile"]
            try:
                automobile = AutomobileVO.objects.get(vin=vin)
                if vin == automobile.vin:
                    new_sale.sold = True
                    new_sale.save()
            except AutomobileVO.DoesNotExist:
                return JsonResponse(
                    {"message": "Automobile Not Found"}, status=404
                )
            return JsonResponse(
                new_sale,
                encoder=SaleEncoder,
                safe=False,
            )
        except Sale.DoesNotExist:
            return JsonResponse(
                {"message": "Sale Object Not Found"}, status=404
            )

####################### SHOW DETAILS/DELETE/UPDATE SALE ###############################

@require_http_methods(["DELETE", "GET"])
def api_show_sales(request, pk):
    if request.method == "GET":
        try:
            sale = Sale.objects.get(id=pk)
            return JsonResponse(
                sale,
                encoder=SaleEncoder,
                safe=False
            )
        except Sale.DoesNotExist:
            return JsonResponse(
                {"message": "Sale Object Not Found"}, status=404
            )
    else:
        if request.method == "DELETE":
            try:
                sale=Sale.objects.get(id=pk)
                sale.delete()
                return JsonResponse(
                    sale,
                    encoder=SaleEncoder,
                    safe=False,
                )
            except Sale.DoesNotExist:
                return JsonResponse(
                    {"message": "Sale Object Not Found; Could not DELETE"}, status=404
                )
