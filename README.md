# CarCar

Team:

* Dylan Noble - Services Microservice, APIs, & Front-End
* Brady Kautsch - Sales Microservice, APIs, & Front-End
* Brady Kautsch and Dylan Noble - Inventory Front-End

## Design + Sales & Services Microservices Explanation

For domain driven design of microservices and front-end capabilities list, please see:

./CarCar_DDD.png
